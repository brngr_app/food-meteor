Restaurants = new Mongo.Collection("restaurants");
Orders = new Mongo.Collection( "orders" );
var activeObjID;

// GENERATE ADD NEW RESTAURANT FORM ..
get_add_form = function()
{
    var html = '<form class="form-horizontal" role="form"><div class="form-group"><div class="col-md-12"><input type="text" class="form-control addFormInput" id="name" placeholder="Restaurant Name"></div></div><div class="form-group"><div class="col-md-12"><input type="number" id="phone" placeholder="Phone Number" class="form-control addFormInput"></div></div><textarea rows="4" class="form-control addFormInput" id="address" placeholder="Address"></textarea></form>';
    return html;
}

// GENERATE ORDER FORM ..
get_order_form = function()
{
    var html = '<form class="form-horizontal" role="form"><div class="form-group"><div class="col-md-12"><select class="form-control orderFormInput" id="restaurant_name">';
    Restaurants.find({}).forEach( function( obj ){
        html += '<option value=' + obj.name + '>' + obj.name + '</option>';
    })

    html += '</select></div></div><div class="form-group"><div class="col-md-12"><textarea rows="4" class="form-control orderFormInput" id="details" placeholder="Order Details"></textarea></div></div></form>';
    return html;
}

// POPUP FORM USING BOOTBOX ..
form_popup = function( title, form, btn1, btn2, callback )
{
    form_elements = {
        title: title,
        message: form,
        callback: function( result ) {
            if ( result == false ) {
                box.modal( 'hide' );
            }
            else {
                callback();
                return false;
            }
        }
    };

    var buttons = {};

    if ( btn1 != '' )
    {
        buttons[ 'cancel' ] = {
            label: btn1,
            className: 'btn-default pull-left'
        };
    }

    if ( btn2 != '' )
    {
        buttons[ 'confirm' ] = {
            label: btn2,
            className: 'btn-primary pull-right'
        };
    }

    form_elements[ 'buttons' ] = buttons;
    var box = bootbox.confirm( form_elements );
}

// SUBMIT ADD NEW RESTAURANT FORM ..
add_restaurant = function(){
    var restaurant = {};
    $( '.addFormInput' ).each( function(){
        restaurant[ $( this ).attr( 'id' ) ] = $( this ).val();
    });
    // ADD DATE ..
    restaurant[ 'createdAt' ] = new Date();
    Restaurants.insert( restaurant );
    $( '.modal' ).modal( 'hide' );
}

// UPDATE RESTAURANT INFORMATION FORM ..
edit_restaurant = function( obj )
{
    activeObjID = obj._id;
    var html = '<form class="form-horizontal" role="form"><div class="form-group"><div class="col-md-12"><input type="text" class="form-control editFormInput" id="newName" value="' + obj.name + '" placeholder="Restaurant Name"></div></div><div class="form-group"><div class="col-md-12"><input type="number" value="' + obj.phone + '" id="newPhone" placeholder="Phone Number" class="form-control editFormInput"></div></div><textarea rows="4" class="form-control editFormInput" id="newAddress" placeholder="Address">' + obj.address + '</textarea></form>';
    return html;
}

hide_popup = function()
{
    $( '.modal' ).modal( 'hide' );
}

// VIEW RESTAURANT ORDERS ..
view_orders = function( obj )
{
    var ordlist = "";
    var count = 0;
    Orders.find({ restaurant_name: obj.name }).forEach( function( obj ){
        ordlist += '<div class="row ordRow"><div class="col-md-12"><p>' + obj.details + '</p></div></div>';
        count++;
    });
    if ( ordlist.trim() == "" ) ordlist = "<h6 class='text-center text-muted'><span class='fa fa-close'></span>&nbsp;&nbsp;No Orders found for <b>" +obj.name+ "</b></h6>";
    form_popup( obj.name + ' orders (' + count + ' Orders)', ordlist, 'Cancel', 'Ok', hide_popup );
}

// UPDATE RESTAURANT'S INFORMATION ..
update_restaurant = function( obj ) {
    Restaurants.update( activeObjID, {
        $set: {
            name: $( '#newName' ).val(),
            phone: $( '#newPhone' ).val(),
            address: $( '#newAddress' ).val(),
        }
    });
    $( '.modal' ).modal( 'hide' );
}

// SUBMIT ORDER ..
submit_order = function() {
    var order = {};
    $( '.orderFormInput' ).each( function(){
        order[ $( this ).attr( 'id' ) ] = $( this ).val();
    });
    Orders.insert( order );
    $( '.modal' ).modal( 'hide' );
}

if (Meteor.isClient) {
  Template.body.helpers({
    restaurants: function(){
        return Restaurants.find({}, {sort: {createdAt: -1}});
    }
  });

  Template.login.events({
    'click #facebook-login': function(event) {
        Meteor.loginWithFacebook({}, function(err){
            if (err) {
                throw new Meteor.Error("Facebook login failed");
            }
        });
    },
 
    'click #logout': function(event) {
        Meteor.logout(function(err){
            if (err) {
                throw new Meteor.Error("Logout failed");
            }
        })
    }
});

  // HANDLE ADDING NEW RESTAURANT ..
    Template.body.events({
        "submit .new-restaurant": function (event) {
        // Prevent default browser form submit
        event.preventDefault();
 
        // Get value from form element
        var name = event.target.name.value;
 
        // Insert a task into the collection
        Restaurants.insert({
            name: name,
            createdAt: new Date() // current time
        });
 
        // Clear form
        event.target.name.value = "";
        },
        "click .add": function( e ) {
            e.preventDefault();
            form_popup( 'New Restaurant', get_add_form(), 'Cancel', 'Add Restaurant', add_restaurant );
        },
        "click .order": function( e ) {
            e.preventDefault();
            form_popup( 'Order Now', get_order_form(), 'Cancel', 'Submit Order', submit_order );
        },
        "click .viewOrders": function( e ) {
            e.preventDefault();
            view_orders( this )
            // console.log( this );
        }
    });

    // SELECTING CHECKBOX AND REMOVING A RESTAURANT ..
    Template.actions.events({
        "click .delete": function () {
        Restaurants.remove(this._id);
        },
        "click .edit": function() {
            form_popup( 'Order Now', edit_restaurant( this ), 'Cancel', 'Update', update_restaurant );
        }
    });

    Template.actions.onRendered( function(){
        this.$( 'button' ).tooltip();
    });
}
